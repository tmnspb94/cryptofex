# [Cryptofex: A Blockchain IDE](https://cryptofex.io)

Issue tracker and wiki for Cryptofex.

## Sending Feedback

If you have a question or suggestion for Cryptofex, please send us an issue.

You can also provide us with information about your platform by going to "Help -> Send Feedback" within Cryptofex.

## How to use Cryptofex

[Instructional Videos](https://www.youtube.com/playlist?list=PLta8B0zskC4G0muA-86r92tckmlCPWuds)

[Documentation](https://gitlab.com/pyrofex/cryptofex/wikis/Official-Documentation)

